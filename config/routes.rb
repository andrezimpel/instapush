Rails.application.routes.draw do

  # get 'user/:id' => 'user#edit', as: "user"
  # get 'user/edit/:id' => 'user#edit', as: "edit_user"
  # post 'user/update'

  devise_for :users
  resources :users


  # relationships
  get 'foreign_relationships/follow_new_people', as: "follow_new_people"
  get 'foreign_relationships/unfollow_current_people', as: "unfollow_current_people"
  resources :foreign_relationships, path: 'foreign-relationships'

  get 'foreign_likes/like_foreign_users_media', as: "like_foreign_users_media"
  resources :foreign_likes, path: 'foreign-likes'


  resources :foreign_users, path: 'foreign-users'


  get '/dashboard' => redirect('/')


  # instagram routes
  get 'oauth/connect', as: 'oauth_connect'
  get 'oauth/callback', as: 'oauth_callback'

  root to: redirect('/foreign-users')

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
