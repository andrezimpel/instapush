set :application, 'instapush'
set :repo_url, 'git@bitbucket.org:andrezimpel/instapush.git'

# whenever
# set :whenever_command, "bundle exec whenever"
# set :whenever_environment, -> { rails_env }
# set :whenever_identifier, -> { "#{application}_#{rails_env}" }
# require "whenever/capistrano"

# delayed job
# require "delayed/recipes"

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# set :deploy_to, '/var/www/my_app'
set :deploy_to, '/var/www/instapush.andrezimpel.com' # production
# set :scm, :git

# user
set :use_sudo, true

set :rvm_ruby_version, '2.1.3p242'
set :rvm_type, :user

set :format, :pretty
set :log_level, :debug
set :pty, true

set :linked_dirs, [
  "log",
  "restricted",
  "public/system",
  "tmp/cache",
  "tmp/pids",
  "tmp/sockets",
  "vendor/bundle",
]

# set :linked_files, %w{config/database.yml}
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # delayed_job
      invoke 'delayed_job:restart'
    end
  end

  after :restart, :restart_passenger do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      within release_path do
        execute :touch, 'tmp/restart.txt'
      end
    end
  end


  after :finishing, 'deploy:restart_passenger'
  after :finishing, 'deploy:cleanup'
  # after :finishing, 'deploy:write_crontab'
end


namespace :delayed_job do

  def args
    fetch(:delayed_job_args, "")
  end

  def delayed_job_roles
    fetch(:delayed_job_server_role, :app)
  end

  desc 'Stop the delayed_job process'
  task :stop do
    on roles(delayed_job_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :'bin/delayed_job', :stop
        end
      end
    end
  end

  desc 'Start the delayed_job process'
  task :start do
    on roles(delayed_job_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :'bin/delayed_job', args, :start
        end
      end
    end
  end

  desc 'Restart the delayed_job process'
  task :restart do
    on roles(delayed_job_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, :'bin/delayed_job', args, :restart
        end
      end
    end
  end
end
