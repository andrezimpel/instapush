class ApplicationController < ActionController::Base
  include Userstamp

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # devise
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :instagram_access_token, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :instagram_access_token, :email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :instagram_access_token, :email, :password, :password_confirmation, :current_password) }
  end

  # instagram
  before_filter :set_instagram_client
  def set_instagram_client

    if current_user
      if current_user.instagram_access_token != nil
        # user has a token
        begin
          client_id = "6d8ea68a22264e4280b0b9a05ba7d38b"
          client_secret = "84f44a76df5347ad8a676cc6a50a3cf3"
          client_ip = "188.226.193.84"

          @instagram_client = Instagram.client(
            client_id: client_id,
            client_secret: client_secret,
            client_ips: client_ip,
            :access_token => current_user.instagram_access_token
          )

          user = @instagram_client.user
        rescue => e
          if e.to_s.include? "The access_token provided is invalid"
            redirect_to oauth_connect_path
          end
        end

        if @instagram_client.access_token != nil
          # names
          if current_user.instagram_username == nil
            current_user.update(instagram_username: user.username)
          end

          if current_user.instagram_full_name == nil
            current_user.update(instagram_full_name: user.full_name)
          end

          # user id
          if current_user.instagram_user_id == nil
            current_user.update(instagram_user_id: user.id)
          end
        end
      end
    end
  end
end
