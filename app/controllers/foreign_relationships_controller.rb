class ForeignRelationshipsController < ApplicationController
  before_action :set_foreign_relationship, only: [:show, :edit, :update, :destroy]

  # GET /foreign_relationships
  # GET /foreign_relationships.json
  def index
    @page_count = params[:page]
    @foreign_relationships = current_user.foreign_relationships.page(@page_count).per(50)
    @all_foreign_relationships = current_user.foreign_relationships
  end

  # GET /foreign_relationships/1
  # GET /foreign_relationships/1.json
  def show
  end

  # GET /foreign_relationships/new
  def new
    @foreign_relationship = ForeignRelationship.new
  end

  # GET /foreign_relationships/1/edit
  def edit
  end

  # POST /foreign_relationships
  # POST /foreign_relationships.json
  def create
    @foreign_relationship = ForeignRelationship.new(foreign_relationship_params)

    respond_to do |format|
      if @foreign_relationship.save
        format.html { redirect_to @foreign_relationship, notice: 'Foreign relationship was successfully created.' }
        format.json { render :show, status: :created, location: @foreign_relationship }
      else
        format.html { render :new }
        format.json { render json: @foreign_relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /foreign_relationships/1
  # PATCH/PUT /foreign_relationships/1.json
  def update
    respond_to do |format|
      if @foreign_relationship.update(foreign_relationship_params)
        format.html { redirect_to @foreign_relationship, notice: 'Foreign relationship was successfully updated.' }
        format.json { render :show, status: :ok, location: @foreign_relationship }
      else
        format.html { render :edit }
        format.json { render json: @foreign_relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /foreign_relationships/1
  # DELETE /foreign_relationships/1.json
  def destroy
    @foreign_relationship.destroy
    respond_to do |format|
      format.html { redirect_to foreign_relationships_url, notice: 'Foreign relationship was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  # follow new people
  def follow_new_people
    # status = ForeignRelationship.delay.follow_new_foreign_users(current_user, @instagram_client)

    UserTasks.schedule!

    # if status == "Found unpublic user."
    #   status = "#{status} " + (link_to "Retry", follow_new_people_path)
    # end
    #notice: status.to_s.html_safe

    # User.follow_hourly_all_users_test

    redirect_to foreign_relationships_path, notice: "Followers will be generated."
  end

  def unfollow_current_people
    UserUnfollowTasks.schedule!
    # ForeignRelationship.unfollow_old_foreign_users(current_user, @instagram_client)
    redirect_to foreign_relationships_path, notice: "You will be unfollowing other users."
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_foreign_relationship
      @foreign_relationship = ForeignRelationship.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def foreign_relationship_params
      params.require(:foreign_relationship).permit(:foreign_user_id, :user_id, :followed_at, :unfollowed_at)
    end
end
