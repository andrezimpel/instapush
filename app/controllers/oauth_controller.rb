class OauthController < ApplicationController
  require "instagram"

  # skip filter
  skip_filter :set_instagram_client


  if Rails.env == "development"
    CALLBACK_URL = "http://instapush.dev/oauth/callback"
  else
    CALLBACK_URL = "http://instapush.andrezimpel.com/oauth/callback"
  end

  Instagram.configure do |config|
    if Rails.env == "development"
      config.client_id = "6d8ea68a22264e4280b0b9a05ba7d38b"
      config.client_secret = "84f44a76df5347ad8a676cc6a50a3cf3"
    else
      config.client_id = "3f40ef6bd4b846c1a25e0a89f6dbbacc"
      config.client_secret = "e6c134839bf84ace9aeff997fe541d0f"
    end
  end

  def connect
    redirect_to Instagram.authorize_url(redirect_uri: CALLBACK_URL, scope: 'basic likes comments relationships')
  end

  def callback
    response = Instagram.get_access_token(params[:code], :redirect_uri => CALLBACK_URL)
    current_user.update(instagram_access_token: response.access_token)
    redirect_to "/dashboard", notice: "You successfully connected to Instagram! :)"
  end
end
