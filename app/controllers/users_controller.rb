class UsersController < ApplicationController
  before_action :user, only: [:edit, :update]

  def edit
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    # update settings
    @user.settings(:instagram_actions).follow_hourly = params[:user][:follow_hourly]
    @user.settings(:instagram_actions).like_hourly = params[:user][:like_hourly]
    @user.settings(:instagram_actions).unfollow_hourly = params[:user][:unfollow_hourly]

    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to edit_user_path(@user), notice: 'Your settings were successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :follow_hourly, :like_hourly, :unfollow_hourly, :username, :instagram_access_token)
    end
end
