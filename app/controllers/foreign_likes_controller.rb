class ForeignLikesController < ApplicationController
  before_action :set_foreign_like, only: [:show, :edit, :update, :destroy]

  # GET /foreign_likes
  # GET /foreign_likes.json
  def index
    @page_count = params[:page]
    @foreign_likes = current_user.foreign_likes.page(@page_count).per(50)
    @all_foreign_likes = current_user.foreign_likes
  end

  # GET /foreign_likes/1
  # GET /foreign_likes/1.json
  def show
  end

  # GET /foreign_likes/new
  def new
    @foreign_like = ForeignLike.new
  end

  # GET /foreign_likes/1/edit
  def edit
  end

  # POST /foreign_likes
  # POST /foreign_likes.json
  def create
    @foreign_like = ForeignLike.new(foreign_like_params)

    respond_to do |format|
      if @foreign_like.save
        format.html { redirect_to @foreign_like, notice: 'Foreign like was successfully created.' }
        format.json { render :show, status: :created, location: @foreign_like }
      else
        format.html { render :new }
        format.json { render json: @foreign_like.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /foreign_likes/1
  # PATCH/PUT /foreign_likes/1.json
  def update
    respond_to do |format|
      if @foreign_like.update(foreign_like_params)
        format.html { redirect_to @foreign_like, notice: 'Foreign like was successfully updated.' }
        format.json { render :show, status: :ok, location: @foreign_like }
      else
        format.html { render :edit }
        format.json { render json: @foreign_like.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /foreign_likes/1
  # DELETE /foreign_likes/1.json
  def destroy
    @foreign_like.destroy
    respond_to do |format|
      format.html { redirect_to foreign_likes_url, notice: 'Foreign like was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def like_foreign_users_media
    LikeImages.schedule!
    redirect_to foreign_likes_path, notice: "Photos will be liked."
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_foreign_like
      @foreign_like = ForeignLike.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def foreign_like_params
      params.require(:foreign_like).permit(:foreign_media_id, :foreing_user_id, :user_id, :liked_at, :unliked_at)
    end
end
