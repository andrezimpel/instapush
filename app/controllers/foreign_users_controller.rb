class ForeignUsersController < ApplicationController
  before_action :set_foreign_user, only: [:show, :edit, :update, :destroy]

  # GET /foreign_users
  # GET /foreign_users.json
  def index
    @page_count = params[:page]
    @foreign_users = current_user.foreign_users.page(@page_count).per(50)
    @all_foreign_users = current_user.foreign_users
  end

  # GET /foreign_users/1
  # GET /foreign_users/1.json
  def show
  end

  # GET /foreign_users/new
  def new
    @foreign_user = ForeignUser.new
  end

  # GET /foreign_users/1/edit
  def edit
  end

  # POST /foreign_users
  # POST /foreign_users.json
  def create
    @foreign_user = ForeignUser.new(foreign_user_params)

    respond_to do |format|
      if @foreign_user.save
        format.html { redirect_to @foreign_user, notice: 'Foreign user was successfully created.' }
        format.json { render :show, status: :created, location: @foreign_user }
      else
        format.html { render :new }
        format.json { render json: @foreign_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /foreign_users/1
  # PATCH/PUT /foreign_users/1.json
  def update
    respond_to do |format|
      if @foreign_user.update(foreign_user_params)
        format.html { redirect_to @foreign_user, notice: 'Foreign user was successfully updated.' }
        format.json { render :show, status: :ok, location: @foreign_user }
      else
        format.html { render :edit }
        format.json { render json: @foreign_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /foreign_users/1
  # DELETE /foreign_users/1.json
  def destroy
    @foreign_user.destroy
    respond_to do |format|
      format.html { redirect_to foreign_users_url, notice: 'Foreign user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_foreign_user
      @foreign_user = ForeignUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def foreign_user_params
      params.require(:foreign_user).permit(:foreign_id, :foreign_username, :foreign_full_name, :media, :follows, :followed_by, :following, :is_private, :not_existing)
    end
end
