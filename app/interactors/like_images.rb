class LikeImages
  include Delayed::RecurringJob
  run_every 30.minutes
  timezone 'Berlin'


  def perform
    # client
    client_id = "6d8ea68a22264e4280b0b9a05ba7d38b"
    client_secret = "84f44a76df5347ad8a676cc6a50a3cf3"
    client_ip = "188.226.193.84"

    # get users
    users = User.all



    # loop users
    for user in users

      # do it for checked users
      if user.settings(:instagram_actions).like_hourly === "true"

        instagram_client = Instagram.client(
          client_id: client_id,
          client_secret: client_secret,
          client_ips: client_ip,
          :access_token => user.instagram_access_token
        )
        relation_action = ForeignLike.like_foreign_users_images(user, instagram_client)
      end
    end
  end
end
