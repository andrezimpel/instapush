module UserHelper

  # boool helper
  def to_boolean(str)
    str = str.to_s
    str == 'true'
  end
end
