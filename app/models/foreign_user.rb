class ForeignUser < ActiveRecord::Base

  has_many :foreign_likes
  has_one :foreign_relationship

  #scopes
  scope :following, lambda { where(['following IS TRUE']) }
  scope :not_private, lambda { where(['is_private IS NOT TRUE']) }
  scope :has_follows, lambda { where(['follows > 0']) }
  scope :has_enough_follows, lambda { where(['follows > 5']) }
  scope :older_than_two_weeks, lambda { where(['created_at < ?', 14.days.ago]) }


  ############################
  #
  # Actions
  #
  ############################

  # create user in system
  def create_foreign_user_in_system(foreign_user_id, client)
    return create_foreign_user_in_system(foreign_user_id, client)
  end
  def self.create_foreign_user_in_system(foreign_user_id, client)

    # look for user
    test_user = ForeignUser.where(foreign_id: foreign_user_id).first

    if test_user != nil
      return test_user
    end

    user = client.user(foreign_user_id)

    user_data = {}
    user_data[:foreign_id] = user.id
    user_data[:foreign_username] = user.username
    user_data[:foreign_full_name] = user.full_name
    user_data[:media] = user.counts.media
    user_data[:follows] = user.counts.follows
    user_data[:followed_by] = user.counts.followed_by

    return foreign_user = ForeignUser.create_with(user_data).find_or_create_by!(foreign_id: user.id)
  end


  # create foreign private user
  def create_foreign_private_user_in_system(foreign_user_id, foreign_username)
    return create_foreign_private_user_in_system(foreign_user_id, foreign_username)
  end
  def self.create_foreign_private_user_in_system(foreign_user_id, foreign_username)
    return ForeignUser.create(foreign_id: foreign_user_id, is_private: true, foreign_username: foreign_username)
  end

  # get follow
  def get_followers(foreign_user_id, client)
    return get_followers(foreign_user_id, client)
  end
  def self.get_followers(foreign_user_id, client)
    return client.user_follows(foreign_user_id)
  end

  # get followed_by
  def get_followed_by(foreign_user_id, client)
    return get_followed_by(foreign_user_id, client)
  end
  def self.get_followed_by(foreign_user_id, client)
    return client.user_followed_by(foreign_user_id)
  end

  # exsisting?
  def existing?
    if self.not_existing === true
      return false
    else
      return true
    end
  end
  def self.existing?
    existing?
  end
end
