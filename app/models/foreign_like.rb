class ForeignLike < ActiveRecord::Base

  belongs_to :foreign_user

  # like images of people the user recently followed with the system
  def self.like_foreign_users_images(current_user, client)
    # get random 5 public foreign users within the last 80
    selected_foreign_users = current_user.foreign_users.not_private.order("created_at DESC").limit(80).sample(5)
    access_token = client.access_token

    # loop foreign users
    for foreign_user in selected_foreign_users do

      # get 3 to 5 recent photos # URL
      foreign_user_media_url = "https://api.instagram.com/v1/users/#{foreign_user.foreign_id}/media/recent/?access_token=#{access_token}"

      # get remote json
      resp = Net::HTTP.get_response(URI.parse(foreign_user_media_url))
      data = resp.body
      data = JSON.parse(data)

      # save photos
      photos = data["data"].take(2)

      # only if we got some
      if photos.count > 0

        # loop photos
        for photo in photos
          photo_id = photo["id"]

          # like photo
          begin
            client.like_media(photo_id)
            # mark photo as liked
            foreignlike = ForeignLike.add_foreign_like(photo_id, foreign_user.foreign_id, foreign_user.id, current_user)
          rescue => e
            return handle_instagram_api_error(e, nil)
          end
        end
      end
    end

    return "Liked some Photos."
  end



  # add foreign relation for the foreign user follow
  def self.add_foreign_like(foreign_media_id, foreign_user_id, user_id, creator)
    ForeignLike.create(
      foreign_media_id: foreign_media_id,
      foreing_user_id: foreign_user_id,
      user_id: user_id,
      liked_at: DateTime.now,
      creator_id: creator.id,
      updater_id: creator.id
    )
  end




  # instagram/faraday error handling
  def self.handle_instagram_api_error(e, *args)
    error_class = e.class
    error_string = e.to_s
    error_return = "An error occured: #{error_class}"
    args = args.first

    logger.debug "ERROR: #{error_class}, #{error_string}, #{args}"

    # internal error
    if error_class == Instagram::InternalServerError
      return error_return = "There was an internal instagram error."
    end

    # too many requests
    if error_class == Instagram::TooManyRequests
      attampts = error_string.split(/ /)[16]
      return error_return = "You've reached the hourly maximum set by Instagram (#{attampts}/20). Try again later."
    end

    # not able to see the user
    if error_class == Instagram::BadRequest
    end
  end

end
