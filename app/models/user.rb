class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]

  # custom login
  def self.find_for_database_authentication(warden_conditions)
   conditions = warden_conditions.dup
   if login = conditions.delete(:login)
     where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
   else
     where(conditions.to_h).first
   end
  end

  def self.find_first_by_auth_conditions(warden_conditions)
  conditions = warden_conditions.dup
  if login = conditions.delete(:login)
    where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
  else
    if conditions[:username].nil?
      where(conditions).first
    else
      where(username: conditions[:username]).first
    end
  end
end

  validates :username, :presence => true, :uniqueness => {
    :case_sensitive => false
  }


  # userstamps
  model_stamper


  # settings
  has_settings do |s|
    s.key :instagram_actions, :defaults => {:follow_hourly => false,
                                            :like_hourly => false,
                                            :unfollow_hourly => false
                                            }
  end

  attr_accessor :follow_hourly, :like_hourly, :unfollow_hourly, :login



  # associations
  has_many :foreign_relationships, foreign_key: :creator_id
  has_many :foreign_likes, foreign_key: :creator_id
  has_many :foreign_users, foreign_key: :creator_id


  # set display name
  def display_name
    if instagram_full_name != nil && instagram_full_name.length > 0
      return instagram_full_name
    elsif instagram_username != nil && instagram_username.length > 0
      return instagram_username
    else
      return email
    end
  end



  # cron actions

  # follow hourly
  def self.follow_hourly_all_users_test
    # client
    client_id = "6d8ea68a22264e4280b0b9a05ba7d38b"
    client_secret = "84f44a76df5347ad8a676cc6a50a3cf3"
    client_ip = "5.10.163.130"

    # get users
    users = User.all



    # loop users
    for user in users

      # do it for checked users
      if user.settings(:instagram_actions).follow_hourly === "true"

        instagram_client = Instagram.client(
          client_id: client_id,
          client_secret: client_secret,
          client_ips: client_ip,
          :access_token => user.instagram_access_token
        )
        relation_action = ForeignRelationship.follow_new_foreign_users(user, instagram_client)
      end
    end
  end
end
