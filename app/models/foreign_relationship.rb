class ForeignRelationship < ActiveRecord::Base

  belongs_to :foreign_user

  # scope
  #scope :for_user, lambda{|user| { :conditions => { :creator_id => user.id, :is_default => true } }
  scope :older_than_two_weeks, lambda { where(['created_at < ?', 14.days.ago]) }
  scope :following, lambda { where(['followed_at IS NOT NULL AND unfollowed_at IS NULL']) }
  scope :unfollowed, lambda { where(['unfollowed_at IS NOT NULL']) }
  scope :not_unfollowed, lambda { where(['unfollowed_at IS NULL']) }


  ############################
  #
  # Actions
  #
  ############################



  def self.follow_new_foreign_users(current_user, client)

    # if the user has already generated followers, start over at that point
    if current_user.foreign_users.count > 0
      temp_instagram_id = current_user.foreign_users.not_private.has_enough_follows.last.foreign_id
    else
      temp_instagram_id = current_user.instagram_user_id
    end

    # get the foreign users the users follows
    user_follows = get_follows(temp_instagram_id, client)

    # loop the foreign users the user follows
    user_follows.each do |user_follow|
      # find or create foreign user
      begin
        foreign_user_follow = find_or_create_foreign_user(user_follow.id, client, current_user)
      rescue => e
        return handle_instagram_api_error(e, instagram_user: user_follow, current_user: current_user, client: client, creator: current_user)
      end

      # found user should be not private
      if foreign_user_follow.is_private === false
        # mark user as followed
        mark_existing_foreign_user_as_followed(foreign_user_follow)


        # get the foreign users the followed foreign user follows
        foreign_user_follows = get_follows(foreign_user_follow.foreign_id, client)

        # loop the foreign users the foreign user follows
        foreign_user_follows.each do |foreign_user_follow|

          # find or create foreign user follow
          begin
            foreign_user_follow_follow = find_or_create_foreign_user(foreign_user_follow.id, client, current_user)
          rescue => e
            return handle_instagram_api_error(e, instagram_user: foreign_user_follow, current_user: current_user, client: client, creator: current_user)
          end


          # follow if user is not follow already
          if foreign_user_follow_follow.following === false && foreign_user_follow_follow.is_private === false

            # check if user has more followed_by than follows
            if foreign_user_follow_follow.follows < foreign_user_follow_follow.followed_by

              # check minimum follows
              if foreign_user_follow_follow.follows > 20

                # follow foreign user follow
                begin
                  follow_foreign_user(foreign_user_follow_follow.foreign_id, client)

                  # add foreign relation for the foreign user follow
                  add_foreign_relationship(foreign_user_follow_follow.foreign_id, foreign_user_follow_follow.id, current_user)

                  # mark user as followed
                  mark_existing_foreign_user_as_followed(foreign_user_follow_follow)
                rescue => e
                  return handle_instagram_api_error(e, foreign_user: foreign_user_follow_follow, current_user: current_user, client: client, creator: current_user)
                end
              end
            end
          end
        end
      end
    end

    return "Successfully followed new users."
  end


  # unfollow old users
  def self.unfollow_old_foreign_users(current_user, client)

    # get foreign relationships
    forein_relationships = ForeignRelationship.where(creator_id: current_user.id).older_than_two_weeks
    forein_relationships = forein_relationships.following.not_unfollowed

    # unfollow
    for foreign_relationship in forein_relationships do
      foreign_user = ForeignUser.where(foreign_id: foreign_relationship.foreign_user_id).first

      # only unfollow existing users
      if foreign_user.existing?

        # unfollow user
        begin
          unfollow_foreign_user(foreign_relationship.foreign_user_id, client)
        rescue => e
          return handle_instagram_api_error(e, foreign_user: foreign_user, current_user: current_user, client: client, creator: current_user, insta_process: "unfollow")
        end

        # mark user as unfollowed
        mark_existing_foreign_user_relationship_as_unfollowed(foreign_user)
      else
        if foreign_relationship.unfollowed_at == nil
          # mark user as unfollowed
          mark_existing_foreign_user_relationship_as_unfollowed(foreign_user)
        end
      end
    end
  end

  # get follows
  def self.get_follows(instagram_user_id, client)
    return client.user_follows(instagram_user_id)
  end


  # get user info
  def self.get_foreign_user_info(instagram_user_id, client)
    return client.user(instagram_user_id)
  end


  # follow foreign user
  def self.follow_foreign_user(instagram_user_id, client)
    return client.follow_user(instagram_user_id)
  end

  # unfollow foreign user
  def self.unfollow_foreign_user(instagram_user_id, client)
    return client.unfollow_user(instagram_user_id)
  end



  #######

  # mark existing foreign user as followed
  def self.mark_existing_foreign_user_as_followed(foreign_user)
    if foreign_user.update(following: true)
      return true
    end
    return false
  end


  # mark existing foreign user as unfollowed
  def self.mark_existing_foreign_user_relationship_as_unfollowed(foreign_user)
    foreign_user_id = foreign_user.foreign_id
    foreign_relationship = ForeignRelationship.where(foreign_user_id: foreign_user_id).first

    foreign_relationship.update(unfollowed_at: DateTime.now)
    foreign_user.update(following: false)
  end


  # add foreign relation for the foreign user follow
  def self.add_foreign_relationship(foreign_user_id, user_id, creator)
    ForeignRelationship.create(
      foreign_user_id: foreign_user_id,
      user_id: user_id,
      followed_at: DateTime.now,
      creator_id: creator.id,
      updater_id: creator.id
    )
  end


  # find or create foreign user
  def self.find_or_create_foreign_user(instagram_user_id, client, creator)
    # check if the foreign user exists
    potential_foreign_user = ForeignUser.where(foreign_id: instagram_user_id).first
    if potential_foreign_user.present?
      return potential_foreign_user
    end


    # get user data
    user_follow_info = get_foreign_user_info(instagram_user_id, client)

    # create new user
    foreign_user_follow = ForeignUser.create(
      foreign_id: user_follow_info.id,
      foreign_username: user_follow_info.username,
      foreign_full_name: user_follow_info.full_name,
      media: user_follow_info.counts.media,
      follows: user_follow_info.counts.follows,
      followed_by: user_follow_info.counts.followed_by,
      creator_id: creator.id,
      updater_id: creator.id
    )
    return foreign_user_follow
  end


  # create not public foreign user in system
  def self.create_unpublic_foreign_user(instagram_user_hash, creator)
    foreign_user_follow = ForeignUser.create(
      foreign_id: instagram_user_hash.id,
      foreign_username: instagram_user_hash.username,
      foreign_full_name: instagram_user_hash.full_name,
      following: false,
      is_private: true,
      creator_id: creator.id,
      updater_id: creator.id
    )
    return foreign_user_follow
  end




  # instagram/faraday error handling
  def self.handle_instagram_api_error(e, *args)
    error_class = e.class
    error_string = e.to_s
    error_return = "An error occured: #{error_class}"
    args = args.first

    logger.debug "ERROR: #{error_class}, #{error_string}, #{args}"

    # internal error
    if error_class == Instagram::InternalServerError
      return error_return = "There was an internal instagram error."
    end

    # too many requests
    if error_class == Instagram::TooManyRequests
      attampts = error_string.split(/ /)[16]
      return error_return = "You've reached the hourly maximum set by Instagram (#{attampts}/20). Try again later."
    end

    # user does not exist anymore or is unfollowed
    if error_string.include? "this user does not exist"
      foreign_user = args[:foreign_user]
      foreign_user.update_attributes(not_existing: true)

      # mark user as unfollowed
      mark_existing_foreign_user_relationship_as_unfollowed(foreign_user)

      return error_return = "This user does not exist anymore."
    end



    # private user
    if error_string.include? "you cannot view this resource"
      # create foreign unpublic user and continue
      create_unpublic_foreign_user(args[:instagram_user], args[:creator])
      # return "Found unpublic user."
      return follow_new_foreign_users(args[:current_user], args[:client])
    end

    # not able to see the user
    if error_class == Instagram::BadRequest && args[:insta_process] != "unfollow"
      # create foreign unpublic user and continue
      create_unpublic_foreign_user(args[:instagram_user], args[:creator])
      # return "Found unpublic user."
      return follow_new_foreign_users(args[:current_user], args[:client])
    end
  end
end
