json.array!(@foreign_likes) do |foreign_like|
  json.extract! foreign_like, :id, :foreign_media_id, :foreing_user_id, :user_id, :liked_at, :unliked_at
  json.url foreign_like_url(foreign_like, format: :json)
end
