json.array!(@foreign_users) do |foreign_user|
  json.extract! foreign_user, :id, :foreign_id, :foreign_username, :foreign_full_name, :media, :follows, :followed_by
  json.url foreign_user_url(foreign_user, format: :json)
end
