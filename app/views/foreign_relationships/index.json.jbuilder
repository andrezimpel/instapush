json.array!(@foreign_relationships) do |foreign_relationship|
  json.extract! foreign_relationship, :id, :foreign_user_id, :user_id, :followed_at, :unfollowed_at
  json.url foreign_relationship_url(foreign_relationship, format: :json)
end
