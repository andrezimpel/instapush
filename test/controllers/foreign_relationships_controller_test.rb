require 'test_helper'

class ForeignRelationshipsControllerTest < ActionController::TestCase
  setup do
    @foreign_relationship = foreign_relationships(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:foreign_relationships)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create foreign_relationship" do
    assert_difference('ForeignRelationship.count') do
      post :create, foreign_relationship: { followed_at: @foreign_relationship.followed_at, foreign_user_id: @foreign_relationship.foreign_user_id, unfollowed_at: @foreign_relationship.unfollowed_at, user_id: @foreign_relationship.user_id }
    end

    assert_redirected_to foreign_relationship_path(assigns(:foreign_relationship))
  end

  test "should show foreign_relationship" do
    get :show, id: @foreign_relationship
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @foreign_relationship
    assert_response :success
  end

  test "should update foreign_relationship" do
    patch :update, id: @foreign_relationship, foreign_relationship: { followed_at: @foreign_relationship.followed_at, foreign_user_id: @foreign_relationship.foreign_user_id, unfollowed_at: @foreign_relationship.unfollowed_at, user_id: @foreign_relationship.user_id }
    assert_redirected_to foreign_relationship_path(assigns(:foreign_relationship))
  end

  test "should destroy foreign_relationship" do
    assert_difference('ForeignRelationship.count', -1) do
      delete :destroy, id: @foreign_relationship
    end

    assert_redirected_to foreign_relationships_path
  end
end
