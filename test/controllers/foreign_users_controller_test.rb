require 'test_helper'

class ForeignUsersControllerTest < ActionController::TestCase
  setup do
    @foreign_user = foreign_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:foreign_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create foreign_user" do
    assert_difference('ForeignUser.count') do
      post :create, foreign_user: { followed_by: @foreign_user.followed_by, follows: @foreign_user.follows, foreign_full_name: @foreign_user.foreign_full_name, foreign_id: @foreign_user.foreign_id, foreign_username: @foreign_user.foreign_username, media: @foreign_user.media }
    end

    assert_redirected_to foreign_user_path(assigns(:foreign_user))
  end

  test "should show foreign_user" do
    get :show, id: @foreign_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @foreign_user
    assert_response :success
  end

  test "should update foreign_user" do
    patch :update, id: @foreign_user, foreign_user: { followed_by: @foreign_user.followed_by, follows: @foreign_user.follows, foreign_full_name: @foreign_user.foreign_full_name, foreign_id: @foreign_user.foreign_id, foreign_username: @foreign_user.foreign_username, media: @foreign_user.media }
    assert_redirected_to foreign_user_path(assigns(:foreign_user))
  end

  test "should destroy foreign_user" do
    assert_difference('ForeignUser.count', -1) do
      delete :destroy, id: @foreign_user
    end

    assert_redirected_to foreign_users_path
  end
end
