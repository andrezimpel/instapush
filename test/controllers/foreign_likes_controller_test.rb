require 'test_helper'

class ForeignLikesControllerTest < ActionController::TestCase
  setup do
    @foreign_like = foreign_likes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:foreign_likes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create foreign_like" do
    assert_difference('ForeignLike.count') do
      post :create, foreign_like: { foreign_media_id: @foreign_like.foreign_media_id, foreing_user_id: @foreign_like.foreing_user_id, liked_at: @foreign_like.liked_at, unliked_at: @foreign_like.unliked_at, user_id: @foreign_like.user_id }
    end

    assert_redirected_to foreign_like_path(assigns(:foreign_like))
  end

  test "should show foreign_like" do
    get :show, id: @foreign_like
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @foreign_like
    assert_response :success
  end

  test "should update foreign_like" do
    patch :update, id: @foreign_like, foreign_like: { foreign_media_id: @foreign_like.foreign_media_id, foreing_user_id: @foreign_like.foreing_user_id, liked_at: @foreign_like.liked_at, unliked_at: @foreign_like.unliked_at, user_id: @foreign_like.user_id }
    assert_redirected_to foreign_like_path(assigns(:foreign_like))
  end

  test "should destroy foreign_like" do
    assert_difference('ForeignLike.count', -1) do
      delete :destroy, id: @foreign_like
    end

    assert_redirected_to foreign_likes_path
  end
end
