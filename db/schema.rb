# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150322085830) do

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 191
    t.string   "queue",      limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "creator_id", limit: 4
    t.integer  "updater_id", limit: 4
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "foreign_likes", force: :cascade do |t|
    t.string   "foreign_media_id", limit: 191
    t.integer  "foreing_user_id",  limit: 4
    t.integer  "user_id",          limit: 4
    t.datetime "liked_at"
    t.datetime "unliked_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "creator_id",       limit: 4
    t.integer  "updater_id",       limit: 4
  end

  create_table "foreign_relationships", force: :cascade do |t|
    t.integer  "foreign_user_id", limit: 4
    t.integer  "user_id",         limit: 4
    t.datetime "followed_at"
    t.datetime "unfollowed_at"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "creator_id",      limit: 4
    t.integer  "updater_id",      limit: 4
  end

  create_table "foreign_users", force: :cascade do |t|
    t.integer  "foreign_id",        limit: 4
    t.string   "foreign_username",  limit: 191
    t.string   "foreign_full_name", limit: 191
    t.integer  "media",             limit: 4
    t.integer  "follows",           limit: 4
    t.integer  "followed_by",       limit: 4
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "creator_id",        limit: 4
    t.integer  "updater_id",        limit: 4
    t.boolean  "following",         limit: 1,   default: false
    t.boolean  "is_private",        limit: 1,   default: false
    t.boolean  "not_existing",      limit: 1
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 191,   null: false
    t.text     "data",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "settings", force: :cascade do |t|
    t.string   "var",         limit: 191,   null: false
    t.text     "value",       limit: 65535
    t.integer  "target_id",   limit: 4,     null: false
    t.string   "target_type", limit: 191,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["target_type", "target_id", "var"], name: "index_settings_on_target_type_and_target_id_and_var", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 191, default: "", null: false
    t.string   "encrypted_password",     limit: 191, default: "", null: false
    t.string   "reset_password_token",   limit: 191
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 191
    t.string   "last_sign_in_ip",        limit: 191
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "creator_id",             limit: 4
    t.integer  "updater_id",             limit: 4
    t.string   "instagram_access_token", limit: 191
    t.string   "instagram_username",     limit: 191
    t.string   "instagram_full_name",    limit: 191
    t.string   "instagram_user_id",      limit: 191
    t.string   "username",               limit: 191
    t.boolean  "not_existing",           limit: 1
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

end
