class CreateForeignLikes < ActiveRecord::Migration
  def change
    create_table :foreign_likes do |t|
      t.integer :foreign_media_id
      t.integer :foreing_user_id
      t.integer :user_id
      t.datetime :liked_at
      t.datetime :unliked_at

      t.timestamps null: false
      t.userstamps
    end
  end
end
