class AddInstagramUsernameToUser < ActiveRecord::Migration
  def change
    add_column :users, :instagram_username, :string
    add_column :users, :instagram_full_name, :string
  end
end
