class CreateForeignUsers < ActiveRecord::Migration
  def change
    create_table :foreign_users do |t|
      t.integer :foreign_id
      t.string :foreign_username
      t.string :foreign_full_name
      t.integer :media
      t.integer :follows
      t.integer :followed_by

      t.timestamps null: false
      t.userstamps
    end
  end
end
