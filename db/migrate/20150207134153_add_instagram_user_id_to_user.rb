class AddInstagramUserIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :instagram_user_id, :string
  end
end
