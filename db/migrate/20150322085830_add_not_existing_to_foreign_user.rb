class AddNotExistingToForeignUser < ActiveRecord::Migration
  def change
    add_column :foreign_users, :not_existing, :boolean
  end
end
