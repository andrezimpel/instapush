class CreateForeignRelationships < ActiveRecord::Migration
  def change
    create_table :foreign_relationships do |t|
      t.integer :foreign_user_id
      t.integer :user_id
      t.datetime :followed_at
      t.datetime :unfollowed_at

      t.timestamps null: false
      t.userstamps
    end
  end
end
