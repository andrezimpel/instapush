class ChangeForeignMediaIdTypeInForeignLikes < ActiveRecord::Migration
  def up
    change_column :foreign_likes, :foreign_media_id, :string
  end

  def down
    change_column :foreign_likes, :foreign_media_id, :integer
  end
end
