class AddFollowingToForeignUser < ActiveRecord::Migration
  def change
    add_column :foreign_users, :following, :boolean, :default => false
  end
end
