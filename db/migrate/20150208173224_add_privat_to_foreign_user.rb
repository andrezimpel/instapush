class AddPrivatToForeignUser < ActiveRecord::Migration
  def change
    add_column :foreign_users, :is_private, :boolean, :default => false
  end
end
