class AddNotExistingToUser < ActiveRecord::Migration
  def change
    add_column :users, :not_existing, :boolean
  end
end
